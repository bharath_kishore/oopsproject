Write a Java program to solve the below real-world problem.

 At an airport, a traveller is allowed entry into the flight only if
he/she clears the following checks: Baggage Check,
Immigration Check, Security Check.
 Create a class Traveller to get all inputs
 Create a class Checks which contains checkBaggage(),
checkImmigration() and checkSecurity() methods. All these
methods should receive an object of Traveller.



 Check Constraints are:
 Check if baggageAmount is greater than or equal to 0
and less than or equal to 40, then the baggage amount
is VALID
 Check if expiryYear is greater than or equal to 2021 and
less than or equal to 2025, immigration is VALID
 Check if nocStatus is TRUE, then security check is
VALID

 In main method, create the object of class Traveller and
initialize the values.
 The created object with values initialized will be passed as
argument to the methods, checkBaggage(), checkImmigration()
and checkSecurity().
 Return values are stored in local variables. If all values are true,
display “Allow Traveller to fly” else, display “Detain Traveller for
Re-checking!”